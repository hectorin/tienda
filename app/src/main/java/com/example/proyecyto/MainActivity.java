package com.example.proyecyto;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm=getSupportFragmentManager();//SIMPRE PONLO PLOX
        Fragment fragment=fm.findFragmentById(R.id.contenedor);
        if(fragment==null){
            fragment=new menu_main();
            fm.beginTransaction()
                    .add(R.id.contenedor,fragment)
                    .commit();
        }
    }
}
